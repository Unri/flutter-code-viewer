import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_highlight/flutter_highlight.dart';
import 'package:flutter_highlight/themes/atom-one-dark.dart';

enum FileRequestStatus { initial, pending, success }

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Code viewer',
      theme: ThemeData.dark(),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  static TextStyle codeStyle =
      const TextStyle(fontSize: 12, fontFamily: 'monospace');
  static double lineNumberItemHeight = 20;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PlatformFile? _currentFile;
  FileRequestStatus _fileRequestStatus = FileRequestStatus.initial;

  void _selectFile() {
    FilePicker.platform
        .pickFiles(
            withData: true,
            onFileLoading: (_) => setState(() {
                  _fileRequestStatus = FileRequestStatus.pending;
                }))
        .then((result) async {
      setState(() {
        if (result != null) {
          _currentFile = result.files.single;
          _fileRequestStatus = FileRequestStatus.success;
        } else {
          _fileRequestStatus = _currentFile == null
              ? FileRequestStatus.initial
              : FileRequestStatus.success;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    String? fileContent;
    int numberOfLines = 0;
    if (_fileRequestStatus == FileRequestStatus.success) {
      try {
        fileContent = const Utf8Decoder().convert(_currentFile!.bytes!);
        numberOfLines = '\n'.allMatches(fileContent).length + 1;
      } on FormatException {
        fileContent = const Utf8Decoder(allowMalformed: true)
            .convert(_currentFile!.bytes!);
        numberOfLines = 0;
      }
    }

    return Scaffold(
        appBar: AppBar(
          title: Text(_currentFile?.name ?? 'Open a file ➔'),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.folder_open),
              onPressed: _selectFile,
            )
          ],
        ),
        body: ({
          FileRequestStatus.initial:
              const Center(child: Text('No file selected')),
          FileRequestStatus.pending:
              const Center(child: Text('Loading file...')),
          FileRequestStatus.success: InteractiveViewer(
              constrained: false,
              child: Row(children: <Widget>[
                if (numberOfLines > 0)
                  SizedBox(
                      height: numberOfLines * MyHomePage.lineNumberItemHeight,
                      width: 50, // Can go up to 5 digits
                      child: ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: numberOfLines,
                          itemBuilder: (_, index) => Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 6),
                              child: SizedBox(
                                  height: MyHomePage.lineNumberItemHeight,
                                  child: Align(
                                      alignment: Alignment.bottomRight,
                                      child: Text('${index + 1}',
                                          style: MyHomePage.codeStyle)))))),
                HighlightView(fileContent ?? '',
                    textStyle: MyHomePage.codeStyle,
                    language: _currentFile?.extension,
                    theme: atomOneDarkTheme)
              ]))
        }[_fileRequestStatus]) // ? Center(child: Text(label))
        );
  }
}
