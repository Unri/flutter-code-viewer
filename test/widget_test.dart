// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'dart:convert';

import 'package:code_viewer/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  const channel = MethodChannel(
      'miguelruivo.flutter.plugins.filepicker', JSONMethodCodec());

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  testWidgets('View UTF8 encoded dart file', (WidgetTester tester) async {
    final fileContent = const Utf8Encoder().convert("print('test')");
    // Mock file pick
    channel.setMockMethodCallHandler((call) async => [
          {
            'path': '/path',
            'name': 'mock-file.dart',
            'bytes': fileContent,
            'size': fileContent.length
          }
        ]);
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MyApp());

    // Trigger file picking
    await tester.tap(find.byIcon(Icons.folder_open));
    await tester.pump();

    // File name title
    expect(find.text('mock-file.dart'), findsOneWidget);
    // Line count
    expect(find.text('1'), findsOneWidget);
    // File content (need to use byWidgetPredicate because the text is in a RichText widget)
    expect(
        find.byWidgetPredicate((widget) =>
            widget is RichText && widget.text.toPlainText() == "print('test')"),
        findsOneWidget);
  });

  testWidgets('Can cancel file picking', (WidgetTester tester) async {
    // Mock cancel file picking
    channel.setMockMethodCallHandler((call) async => null);
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MyApp());

    // Trigger file picking
    await tester.tap(find.byIcon(Icons.folder_open));
    await tester.pump();

    // Initial title
    expect(find.text('Open a file ➔'), findsOneWidget);
    // No Line count
    expect(find.text('1'), findsNothing);
    // No file content
    expect(find.text('No file selected'), findsOneWidget);
  });

  testWidgets('Keep last opened file when cancelling file picking',
      (WidgetTester tester) async {
    final fileContent = const Utf8Encoder().convert("print('test')");
    channel.setMockMethodCallHandler((call) async => [
          {
            'path': '/path',
            'name': 'mock-file.dart',
            'bytes': fileContent,
            'size': fileContent.length
          }
        ]);
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MyApp());

    await tester.tap(find.byIcon(Icons.folder_open));
    await tester.pump();

    // Mock cancel file picking
    channel.setMockMethodCallHandler((call) async => null);

    // Trigger file picking
    await tester.tap(find.byIcon(Icons.folder_open));
    await tester.pump();

    // File name title
    expect(find.text('mock-file.dart'), findsOneWidget);
    // Line count
    expect(find.text('1'), findsOneWidget);
    // File content (need to use byWidgetPredicate because the text is in a RichText widget)
    expect(
        find.byWidgetPredicate((widget) =>
            widget is RichText && widget.text.toPlainText() == "print('test')"),
        findsOneWidget);
  });
}
